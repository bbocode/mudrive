#ifndef MUDRIVE_CONFIG_HH_
#define MUDRIVE_CONFIG_HH_

#include <Arduino.h>

#include "mud/utils.hh"

/*********************************************************************************************************************************
 * HARDWARE CONFIG
 *********************************************************************************************************************************/

//=== analog
#define MUD_ANALOG_REFERENCE_TYPE INTERNAL
#define MUD_ANALOG_REFERENCE_VOLTAGE 1.2F       // Teensy 3.6
#define MUD_ANALOG_RESOLUTION 12                // Teensy 3.6

// internal
#define __MUD_ANALOG_MIN (0)                                                     // analog minimum value
#define __MUD_ANALOG_MAX (mud::Pow<2,MUD_ANALOG_RESOLUTION>::result-1)           // analog maximum value (2^n-1)
#define __MUD_ANALOG_F (MUD_ANALOG_REFERENCE_VOLTAGE / float(__MUD_ANALOG_MAX))  // analog integer to volt conversion factor

namespace mud {
    namespace config {
        struct LED {
            struct pin {
                static constexpr const uint8_t blue = 32; //B
                static constexpr const uint8_t green = 31; //G
                static constexpr const uint8_t red = 30; //R
            };
        };

        struct drive {
            struct pin {
                static constexpr const uint8_t dir = 35;
                static constexpr const uint8_t clk = 36;
                static constexpr const uint8_t voltage = A22;
            };

            struct clk {
                static constexpr const uint8_t edge = FALLING;
            };

            struct dir {
                static constexpr const uint8_t forward = HIGH;
            };
        };

        struct sense {
            struct pin {
                static constexpr const uint8_t supply_voltage = A0;
                static constexpr const uint8_t drive_current = A1;
            };
        };

        struct button {
            struct pin {
                static constexpr const uint8_t reset = 12;

                static constexpr const uint8_t up = 6;
                static constexpr const uint8_t down = 7;

                static constexpr const uint8_t select = 11; // dial press
                static constexpr const uint8_t dial_a = 10;
                static constexpr const uint8_t dial_b = 9;
            };
        };
    }
}

/*********************************************************************************************************************************
 * COMPILE TIME CONFIGURATION
 *********************************************************************************************************************************/

// FIXME
#if !defined(DEBUG)
#define DEBUG
#endif

/** Pin of the development-board LED.
 * Use the dev-board LED to display an active "heart beat" (see setup()/loop()). */
#define __MUD_HAS_BOARD_LED 13

/** String buffer size for SSO. */
#define MUD_STRING_SMALL_SIZE 16

#endif /* CONFIG_HH_ */
