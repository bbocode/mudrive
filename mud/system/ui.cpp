#include "ui.hh"
#include "mud/version.hh"
#include "mud/settings.hh"
#include "mud/system/buttons.hh"
#include "mud/system/sense.hh"
#include "mud/system/stepper.hh"

using namespace mud;

using buttons = mud::system::buttons;
using sense = mud::system::sense;
using stepper = mud::system::stepper;

__MUD_DISPLAY_CLASS mud::system::ui::screen = __MUD_DISPLAY_CLASS(__MUD_DISPLAY_ARGS);
const mud::system::ui::dimension mud::system::ui::layout = { __MUD_DISPLAY_WIDTH, __MUD_DISPLAY_HEIGHT };
mud::system::ui::progress_values mud::system::ui::__progress = { 0, 0, false };
system::ui::context system::ui::__context = system::ui::context::MAIN;

void system::ui::setup() {
    __MUD_DISPLAY_SETUP(screen);

    // show banner
    {
        scope frame;

        set_text_size(2);
        set_text_color(color::white);
        puttext({0, 0, origin::center}, align::bottom|align::hcenter, "Z-Drive\n");

        set_text_size(1);
        puttext({0, 0, origin::center}, align::top|align::hcenter, "Release " MUD_RELEASE_S "\n");
    }

    // assure, that banner is visible for a while
    delay(1000);
}

void system::ui::step() {
    scope full_redraw;

    switch(__context) {
        case context::MAIN:
            main::handle();
            break;
    }

    __drawprogress();
}

// ================================================================================================================================
void system::ui::__drawprogress() {
#define __MUD_PROGRESS_BAR_WIDTH __MUD_DISPLAY_WIDTH/4
#define __MUD_PROGRESS_BAR_STEP 4
    if (__progress.unknown) {
        // here, N is the center position, moving left to right and back.
        // N's sign denotes the direction

        // NOTE: this relies on
        //           __MUD_DISPLAY_WIDTH % __MUD_PROGRESS_BAR_STEP == 0
        //       !!

        int dir = __progress.N < 0 ? -1 : 1;
        int offset = __progress.N;

        // draw moving bar element
        screen.drawFastHLine(dir*offset-__MUD_PROGRESS_BAR_WIDTH/2, __MUD_DISPLAY_HEIGHT-1,
                __MUD_PROGRESS_BAR_WIDTH,
                std::underlying_type_t<color>(color::inverse));

        // update values
        offset += __MUD_PROGRESS_BAR_STEP;
        if (offset >= __MUD_DISPLAY_WIDTH)
            __progress.N = -__MUD_DISPLAY_WIDTH;
        else
            __progress.N = offset;
    }
    else {
        // NOTE: this relies on
        //              __MUD_DISPLAY_WIDTH * __progress.value
        //       not overflowing!

        screen.drawFastHLine(0, __MUD_DISPLAY_HEIGHT - 1,
                (__MUD_DISPLAY_WIDTH * __progress.value) / __progress.N,
                std::underlying_type_t<color>(color::inverse));
    }
}

int system::ui::vputtext(position pos, align align_, const char *fmt, va_list args) {
    char buf[256];
    int retval = vsnprintf(buf, 256, fmt, args);

    // calculate alignment
    int16_t x,y;
    uint16_t w,h;
    screen.getTextBounds(buf, 0, 0, &x, &y, &w, &h);
    if (!!(align_ & align::right)) {
        if (!!(align_ & align::left))
            pos.x -= w/2;
        else
            pos.x -= w;
    }
    if (!!(align_ & align::bottom)) {
        if (!!(align_ & align::top))
            pos.y -= h/2;
        else
            pos.y -= h;
    }

    // draw
    set_cursor(pos);
    screen.print(buf);
    return retval;
}

// === PAGES ======================================================================================================================
system::ui::main::select system::ui::main::__selected = system::ui::main::select::speed;

void system::ui::main::handle() {
    //=== BUTTONS
    if (buttons::select) {
        int delta = buttons::encoder;

        if (delta) {
            // things to do when leaving last selected
            switch(__selected) {
                case select::strength:
                    // switch on automatic drive strength handling
                    stepper::set_drive_mode(stepper::mode::automatic);
                    break;

                default:
                    break;
            }

            // update selected
            __selected = __selected + delta;

            // things to do when entering new selected
            switch(__selected) {
                case select::strength:
                    // switch off automatic drive strength handling
                    stepper::set_drive_mode(stepper::mode::manual);
                    break;

                default:
                    break;
            }
        }
    }
    else {
        switch (__selected) {
            case select::speed: { // change drive speed
                float drive_speed = stepper::get_speed();

                int delta = buttons::encoder;
                int dir = sgn(delta);
                for (; delta; delta -= dir) {
                    // in-/decrement within decades
                    float digit = floor(log10(drive_speed + 0.0001)); // digit to modify
                    float offset = pow(10.0F, digit); // value to add/subtract

                    if (dir < 0) {
                        if (offset > drive_speed/1.2F) // correct for 100 -> 90 transition
                            offset *= 0.1F;

                        drive_speed -= offset;
                    }
                    else
                        drive_speed += offset;

                    // round to nearest n*offset
                    drive_speed = round(drive_speed/offset) * offset;

                    // commit
                    stepper::set_speed(drive_speed);
                }


                break;
            }
            case select::strength: { // change drive strength
                // increment/decrement 5%-wise
                int delta = buttons::encoder;
                if (delta) {
                    float drive_strength = stepper::get_drive_strength();

                    drive_strength += float(delta)*0.05F;
                    drive_strength = round(drive_strength * 20.0F) * 0.05F;

                    if (drive_strength < 0.0F) drive_strength = 0.0F;
                    else if (drive_strength > 1.0F) drive_strength = 1.0F;

                    stepper::set_drive_strength(drive_strength);
                }

                break;
            }

            case select::__last:
                // must not happen
                return;
        }
    }

    //=== DISPLAY
    // draw "highlights"
    switch (__selected) {
        case select::speed:
            fillrect(text_geometry(0,4), color::white);
            break;
        case select::strength:
            fillrect(text_geometry(0,5), color::white);
            break;

        case select::__last:
            // must not happen
            return;
    }

    // draw text
    set_text_color(color::inverse);
    puttext(text_position(0,0), align::topleft, "    X  -----.--- um");
    puttext(text_position(0,1), align::topleft, "    Y  -----.--- um");
    puttext(text_position(0,2), align::topleft, "    Z %+010.3f um", stepper::get_position());

    puttext(text_position(0,4), align::topleft, "Speed  % 9.3f um/s", stepper::get_speed());
    puttext(text_position(0,5), align::topleft, "Drive    %5.1f %%", stepper::get_drive_strength() * 100.0F);
    puttext(text_position(0,6), align::topleft, "        %6.1f mV", sense::supply_voltage() * 1000.0F);
    puttext(text_position(0,7), align::topleft, "        %6.1f mA", sense::drive_current() * 1000.0F);
}

