#include "stepper.hh"
#include "mud/system/buttons.hh"
#include "mud/system/led.hh"
#include "mud/system/protocol.hh"
#include "mud/system/ui.hh"
#include "mud/system/sense.hh"
#include "mud/settings.hh"

using namespace mud;

using io = mud::system::protocol;
using sense = mud::system::sense;
using ui = mud::system::ui;

void system::stepper::setup() {
    // read configuration
    settings cfg;

    __drive_frequency_max = cfg["drive.frequency.max"];
    __drive_frequency_min = cfg["drive.frequency.min"];

    __drive_voltage_factor = cfg["drive.voltage.factor"];
    __drive_voltage_offset = cfg["drive.voltage.offset"];

    int count_per_rotation = ((int) cfg["motor.phases"]) * ((int) cfg["drive.steps_per_pole"]) * ((int) cfg["motor.gear_ratio"]);

    __step_to_position = ((float) cfg["spindle.pitch"]) * ((float) cfg["objective.lever_ratio"]) / float(count_per_rotation);
    __position_to_step = float(count_per_rotation) / (((float) cfg["spindle.pitch"]) * ((float) cfg["objective.lever_ratio"]));

    // prepare
    analogWriteResolution(MUD_ANALOG_RESOLUTION);
    analogWrite(config::drive::pin::voltage, 0);

    pinMode(pin.dir, OUTPUT_OPENDRAIN);
    pinMode(pin.clk, OUTPUT_OPENDRAIN);

    __setdirection(1);
    __clockreturn();

    __setup();

    // prepare
    set_speed(cfg["objective.speed.max"]);
    set_drive_strength(1.0F);
}

void system::stepper::step() {
    // this function is called with 50 Hz (see rate)
    static unsigned int idlecount = 0;
#define AUTOSLEEP (rate * 4)  /* go to sleep after 4 secs */
#define SLEEPVAL 0.2F       /* 20% of max power in sleep */

    if (__drive_mode == mode::manual) {
        __drive_state = state::ready;
        return;
    }

    double strengthinc = 0.01F;
    switch (__drive_state) {
        case state::moving:
            idlecount = 0;
            break;

        case state::wait:
        case state::stop:
            // do nothing
            break;

        case state::sleep: {
            float strength = get_drive_strength() - strengthinc;
            if (strength < SLEEPVAL)
                strength = SLEEPVAL;

            __set_drive_strength(strength);
            break;
        }

        case state::boot: {
            // calibrate current offset
#define OFFSET_AVG_N 100
            static int N = 0;
            static float calib_current_offset = 0.0F;

            // switch off drive (to be sure)
            analogWrite(config::drive::pin::voltage, 0);

            // read current
            float current;
            for(int i=0; i<__MUD_ANALOG_AVERAGE_N; ++i)
                current = sense::drive_current();

            calib_current_offset += current;

            // update progress
            ui::progress(N, OFFSET_AVG_N);

            if (++N == OFFSET_AVG_N) {
                // set offset
                sense::__drive_current_calib.offset = calib_current_offset / float(N);

                // switch off progress, proceed
                ui::progress(0);
                __drive_state = state::wakeup;
            }
            else
                break;
        }
        case state::wakeup: {
            float strength = get_drive_strength() + strengthinc;

            if (strength >= 1.0F) {
                __set_drive_strength(1.0F);
                __drive_state = state::ready;
            }
            else
                __set_drive_strength(strength);

            break;
        }

        case state::ready:
            ++idlecount;
            if ((__drive_mode == mode::automatic) && (idlecount > AUTOSLEEP))
                __drive_state = state::sleep;
            break;
    }
}


IntervalTimer system::stepper::__timer;
unsigned int system::stepper::__period = 1000;

void system::stepper::isr() {
    switch (__drive_state) {
        case state::ready:
        case state::moving:
        case state::stop:
            break;

        case state::sleep:
            // wake-up if moving is required
            if ((__drive_target != __drive_count) || (system::buttons::up || system::buttons::down))
                __drive_state = state::wakeup;

        default:
            return;
    }

    if ((system::buttons::reset) || (__drive_state == state::stop)) {
        __drive_target = __drive_count;
        __clockreturn();

        __drive_state = state::ready;
        system::LED::active(system::LED::pattern::off);
    }
    else if (__drive_target != __drive_count) {
        __drive_state = state::moving;
        __clocktick();
        if (__drive_target == __drive_count) {
            __drive_state = state::ready;
            system::LED::active(system::LED::pattern::off);
        }
    }
    else if (system::buttons::up != system::buttons::down) {
        __drive_state = state::moving;
        system::LED::active(system::LED::pattern::on);

        // update direction
        if (system::buttons::up)
            __setdirection(1);
        else
            __setdirection(-1);

        // increment
        __clocktick();
        __drive_target = __drive_count;
    }
    else {
        __clockreturn();
        __drive_state = state::ready;
        system::LED::active(system::LED::pattern::off);
    }
}

volatile float system::stepper::__drive_frequency_max;
volatile float system::stepper::__drive_frequency_min;

volatile float system::stepper::__drive_voltage_factor;
volatile float system::stepper::__drive_voltage_offset;

volatile int system::stepper::__drive_dir = 0;
volatile int system::stepper::__drive_count = 0;
volatile int system::stepper::__drive_target = 0;

volatile system::stepper::mode system::stepper::__drive_mode = system::stepper::mode::automatic;
volatile system::stepper::state system::stepper::__drive_state = system::stepper::state::boot;

volatile uint16_t system::stepper::__drive_strength = 0;

float system::stepper::__step_to_position = 0.0F;
float system::stepper::__position_to_step = 0.0F;

float system::stepper::get_drive_strength() {
    float Vcc_half = sense::supply_voltage()/2.0F;
    float Vm = __drive_voltage_factor * float(__drive_strength) + __drive_voltage_offset;

    // BLD 05002 S datasheet:
    //          Vout = (Vm - Vcc/2) / sqrt(2)
    // is the effective output voltage.

    if (Vm < Vcc_half)
        return 0.0F;
    else
        return pow(Vm/Vcc_half-1, 2);
}

void system::stepper::__set_drive_strength(float value) {
    if (value <= 0.000001F)
        __drive_strength = 0;
    else {
        float Vcc_half = sense::supply_voltage()/2.0F;
        float Vm = (sqrt(value)+1) * Vcc_half;

        __drive_strength = (Vm - __drive_voltage_offset)/__drive_voltage_factor;
    }

    analogWrite(config::drive::pin::voltage, __drive_strength);
}

void system::stepper::set_speed(float v) {
    settings cfg;
    if (v < (float) cfg["objective.speed.min"])
        v = cfg["objective.speed.min"];
    else if (v > (float) cfg["objective.speed.max"])
        v = cfg["objective.speed.max"];

    __setfrequency(v * __position_to_step);
}

void system::stepper::set_center() {
    io::log("set_center\n");

    // FIXME this must be handled in the isr()

    // get 'center position' from config
    settings cfg;
    float centerpos = cfg["objective.center"];

    // remember speed, and set to fastest
    float curspeed = get_speed();
    set_speed(cfg["objective.speed.max"]);

    // move up (into block), then down
    float tmp = set_position(4*centerpos);
    for(; tmp != get_position(););
    set_zero();
    tmp = set_position(-centerpos);
    for(; tmp != get_position(););
    set_zero();

    // recall speed
    set_speed(curspeed);
}

