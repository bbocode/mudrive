#ifndef MUD_SYSTEM_BUTTONS_HH_
#define MUD_SYSTEM_BUTTONS_HH_

#include <Arduino.h>
#include "config.hh"
#include "mud/utils.hh"
#include "mud/button.hh"

namespace mud {
    namespace system {

        /** Buttons.
         * FIXME
         */
        class buttons {
        public:
            static constexpr const int rate = 50;

        public:
            static button::press<config::button::pin::reset> reset;
            static button::press<config::button::pin::select> select;
            static button::press<config::button::pin::up> up;
            static button::press<config::button::pin::down> down;

            typedef button::encoder<config::button::pin::dial_a, config::button::pin::dial_b> encoder_type;
            static encoder_type encoder;

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

        public:
        };
    }
}

#endif /* MUD_SYSTEM_HAPTIC_HH_ */
