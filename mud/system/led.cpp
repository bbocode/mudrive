#include "led.hh"

using namespace mud;

constexpr const uint8_t system::LED::pin[N];
uint16_t system::LED::__shift = 1;
volatile system::LED::pattern system::LED::__pattern[N] = { system::LED::pattern::on };

void system::LED::setup() {
    for(int i=0; i<N; ++i) {
        pinMode(pin[i], OUTPUT);
        __write(i, true);
    }
}

void system::LED::step() {
    // increment pattern bit
    if (!(__shift <<= 1))
        __shift = 1;

    // switch LED(s) on/off
    for(int i=0; i<N; ++i)
        __write(i, std::underlying_type_t<pattern>(__pattern[i]) & __shift);
}

