#ifndef MUD_ANALOG_HH_
#define MUD_ANALOG_HH_

#include <Arduino.h>

#include "config.hh"

namespace mud {
    namespace analog {
        /** ADC/DAC calibration.
         *
         * The values shall convert ADC/DAC integer values to voltage [V] and vice versa.
         *
         * */
        struct calibration {
            float factor;
            float offset;
        };

        /** Read voltage [V] from respective ADC. */
        inline float read(int pin, const calibration &calib) {
            return float(analogRead(pin)) * calib.factor + calib.offset;
        }

        /** Set voltage [V] on respective DAC. */
        inline void write(int pin, float V, const calibration &calib) {
            analogWrite(pin, (V-calib.offset)*calib.factor);
        }
    }
}

#endif /* MUD_ANALOG_HH_ */
