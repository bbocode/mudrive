#ifndef MUD_ASSERT_HH_
#define MUD_ASSERT_HH_

#include "config.hh"

#if defined(DEBUG)
// NOTE: as the current Teensy-LED is blinking (unless an assertion is hit), there's no need to send
//       any additional information - debugging without JTAG/SWD etc. interface is hard anyway /:)
#define assert(expr) \
    if (!(expr)) { \
        for(;;); \
    }
#else
#define assert(expr) (expr)
#endif

#endif /* MUD_ASSERT_HH_ */
