#include "sense.hh"

#include "mud/settings.hh"

using namespace mud;

analog::calibration mud::system::sense::__supply_voltage_calib;
analog::calibration mud::system::sense::__drive_current_calib;

void system::sense::setup() {
    analogReadResolution(MUD_ANALOG_RESOLUTION);
    analogReference(MUD_ANALOG_REFERENCE_TYPE);

    settings cfg;

    for(int i = 0; i<__MUD_ANALOG_AVERAGE_N; ++i)
        __supply_voltage += (__supply_voltage_buf[i] = analogRead(config::sense::pin::supply_voltage));
    __supply_voltage_calib.factor = cfg["sense.supply_voltage.factor"];
    __supply_voltage_calib.offset = cfg["sense.supply_voltage.offset"];

    for(int i = 0; i<__MUD_ANALOG_AVERAGE_N; ++i)
        __drive_current += (__drive_current_buf[i] = analogRead(config::sense::pin::drive_current));
    __drive_current_calib.factor = cfg["sense.drive_current.factor"];
    __drive_current_calib.offset = cfg["sense.drive_current.offset"];
}

int system::sense::__supply_voltage = 0;
int system::sense::__drive_current = 0;
int system::sense::__index = 0;
int system::sense::__supply_voltage_buf[__MUD_ANALOG_AVERAGE_N];
int system::sense::__drive_current_buf[__MUD_ANALOG_AVERAGE_N];

void system::sense::step() {
    // record rolling average
    __supply_voltage -= __supply_voltage_buf[__index];
    __supply_voltage_buf[__index] = analogRead(config::sense::pin::supply_voltage);
    __supply_voltage += __supply_voltage_buf[__index];

    __drive_current -= __drive_current_buf[__index];
    __drive_current_buf[__index] = analogRead(config::sense::pin::drive_current);
    __drive_current += __drive_current_buf[__index];

    if (++__index >= __MUD_ANALOG_AVERAGE_N) __index = 0;
}

float system::sense::supply_voltage() {
    float retval = float(__supply_voltage) / float(__MUD_ANALOG_AVERAGE_N);
    retval *= __supply_voltage_calib.factor;
    retval -= __supply_voltage_calib.offset;
    return retval;
}

float system::sense::drive_current() {
    float retval = float(__drive_current) / float(__MUD_ANALOG_AVERAGE_N);
    retval *= __drive_current_calib.factor;
    retval -= __drive_current_calib.offset;
    return retval;
}
