#ifndef MUD_SYSTEM_UI_HH_
#define MUD_SYSTEM_UI_HH_

#include "config.hh"

#include <Adafruit_GFX.h>

#ifdef USE_ADAFRUIT_SSD1306_I2C
# include <Wire.h>
# include <Adafruit_SSD1306.h>

# define __MUD_DISPLAY_WIDTH 128                        ///< Screen width [px]
# define __MUD_DISPLAY_HEIGHT 64                        ///< Screen height [px]

# define __MUD_DISPLAY_MONOCHROME                       ///< This is a monochrome display.

# define __MUD_DISPLAY_COLOR_BLACK SSD1306_BLACK        ///< Draw 'off' pixels
# define __MUD_DISPLAY_COLOR_WHITE SSD1306_WHITE        ///< Draw 'on' pixels
# define __MUD_DISPLAY_COLOR_INVERSE SSD1306_INVERSE    ///< Invert pixels

# if !defined(__MUD_DISPLAY_RESET)
#  define __MUD_DISPLAY_RESET   2                       ///< Screen reset pin (or -1 if sharing reset pin)
# endif

#define __MUD_DISPLAY_I2C 0x3D                          ///< Default I2C port on SSD1306

#define __MUD_DISPLAY_CLASS Adafruit_SSD1306
#define __MUD_DISPLAY_ARGS  __MUD_DISPLAY_WIDTH, __MUD_DISPLAY_HEIGHT, &Wire, __MUD_DISPLAY_RESET

#define __MUD_CHARACTER_HEIGHT 8                        ///< Hard-coded character height.
#define __MUD_CHARACTER_WIDTH 8                         ///< Hard-coded character width.

inline void __zdrive_adafruit_ssd1306_setup(Adafruit_SSD1306 &display) {
    // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
    if (!display.begin(SSD1306_SWITCHCAPVCC, __MUD_DISPLAY_I2C)) {
        // FIXME use 'abort'-like function
        for (;;)
            ; // Don't proceed, loop forever
    }

    // use full CP437 character set
    display.cp437(true);
}

#define __MUD_DISPLAY_SETUP(DISPLAY) __zdrive_adafruit_ssd1306_setup(DISPLAY);

#else
#error
#endif

namespace mud {
    namespace system {

        /** User interface.
         * FIXME
         */
        // FIXME separate this into a base class and the actual current GUI
        class ui {
        public:
            enum class color : int {
#if defined(__MUD_DISPLAY_MONOCHROME)
                black = __MUD_DISPLAY_COLOR_BLACK, white = __MUD_DISPLAY_COLOR_WHITE, inverse = __MUD_DISPLAY_COLOR_INVERSE,
#endif
            };

            /** Text alignment. */
            enum class align : int {
                left = 1, right = 2,

                hcenter = 3,

                top = 4, bottom = 8,

                vcenter = 12,

                topleft = left | top, center = 15,
            };

            /** Coordinate origin.
             * FIXME
             * */
            typedef align origin;

            /** Position on display screen. */
            class position {
            public:
                int x = 0;
                int y = 0;

            public:
                constexpr position() = default;
                constexpr position(int x_, int y_)
                        : x(x_), y(y_) {
                }
                constexpr position(int x_, int y_, origin origin_)
                        : x(x_), y(y_) {
                    typedef std::underlying_type_t<origin> origin_type;

                    origin_type _origin = origin_type(origin_);
                    if (_origin & origin_type(origin::right)) {
                        if (_origin & origin_type(origin::left))
                            x += layout.w / 2;
                        else
                            x += layout.w;
                    }
                    if (_origin & origin_type(origin::bottom)) {
                        if (_origin & origin_type(origin::top))
                            y += layout.h / 2;
                        else
                            y += layout.h;
                    }
                }
                constexpr position(int p[2])
                        : x(p[0]), y(p[1]) {
                }

            public:
                constexpr position& operator+=(const position &other) {
                    x += other.x;
                    y += other.y;
                    return *this;
                }

                constexpr position& operator-=(const position &other) {
                    x -= other.x;
                    y -= other.y;
                    return *this;
                }

                constexpr position& operator*=(int n) {
                    x *= n;
                    y *= n;
                    return *this;
                }

                constexpr position& operator/=(int n) {
                    x /= n;
                    y /= n;
                    return *this;
                }
            };

            /** Dimensions */
            class dimension {
            public:
                /// marker for entire width or height (comparable to string::npos).
                static constexpr const unsigned int all = ((unsigned int) -1);

            public:
                unsigned int w = all;
                unsigned int h = all;

            public:
                constexpr dimension() = default;
                constexpr dimension(unsigned int w_, unsigned int h_)
                        : w(w_), h(h_) {
                }
                constexpr dimension(unsigned int p[2])
                        : w(p[0]), h(p[1]) {
                }
//                constexpr dimension(int p[2])
//                        : w(reinterpret_cast<unsigned int>(p[0])), h(reinterpret_cast<unsigned int>(p[1])) {
//                }
            };

            /** Geometry. */
            class geometry: public position, public dimension {
            public:
                constexpr geometry() = default;
                constexpr geometry(int x_, int y_)
                        : position(x_, y_) {
                }
                constexpr geometry(int x_, int y_, unsigned int w_, unsigned int h_)
                        : position(x_, y_), dimension(w_, h_) {
                }
            };

            /** Frame scope.
             * When doing a full redraw of the screen, there are three steps to be done:
             * 1. clear the screen buffer
             * 2. draw everything (to the buffer)
             * 3. commit drawn frame to the display
             *
             * This class does 1 and 3 automatically within a local scope.
             *
             * USAGE:
             * @code
             * {
             *      mud::system::ui::scope frame;
             *
             *      // draw everything here
             * }
             * @endcode
             *  */
            class scope {
            public:
                scope() {
                    ui::clear();
                }

                ~scope() {
                    ui::commit();
                }
            };

        public:
            /// The (e.g. OLED) display.
            static __MUD_DISPLAY_CLASS screen;

            ///< Screen layout (dimensions in pixels).
            static const dimension layout;

        public:
            ui() = default;

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

            //=== GENERAL
        public:
            /** Clear display buffer. */
            static void clear() {
                screen.clearDisplay();
            }

            /** Commit display (copy frame buffer to display). */
            static void commit() {
                screen.display();
            }

            //=== PROGRESS
        protected:
            static struct progress_values {
                int value;
                int N;
                bool unknown;
            } __progress;

            static void __drawprogress();

        public:
            /** Set progress.
             * @param val Progress value (in relation to @a N).
             * @param N Progress finish count.
             *
             * FIXME
             *
             * */
            static void progress(int val = -1, int N = 0) {
                if (val == 0) {
                    __progress.value = 0;
                    __progress.N = N;
                    __progress.unknown = false;
                }
                else if (val < 0) {
                    __progress.N = 0;
                    __progress.unknown = true;
                }
                else {
                    __progress.value = val;
                    if (N > 0)
                        __progress.N = N;
                }
            }

            //=== GFX
        public:
            /** Set current drawing position. */
            static void set_cursor(position pos) {
                screen.setCursor(pos.x, pos.y);
            }

            /** Set text color. */
            static void set_text_color(color col) {
                screen.setTextColor(std::underlying_type_t<color>(col));
            }

            /** Set text size. */
            static void set_text_size(unsigned int s) {
                screen.setTextSize(s);
            }

            static void set_text_size(unsigned int sx, unsigned int sy) {
                screen.setTextSize(sx, sy);
            }

            /** Get character-based cursor position. */
            static constexpr position text_position(int x, int y, origin origin_ = origin::topleft) {
                return {x*__MUD_CHARACTER_WIDTH, y * __MUD_CHARACTER_HEIGHT, origin_};
            }

            /** Get character-based dimensions. */
            static constexpr geometry text_geometry(int x, int y, unsigned int w = geometry::all, unsigned int h = 1) {
                if (w < geometry::all)
                    w *= __MUD_CHARACTER_WIDTH;
                if (h < geometry::all)
                    h *= __MUD_CHARACTER_HEIGHT;
                return {x*__MUD_CHARACTER_WIDTH, y * __MUD_CHARACTER_HEIGHT, w, h};
            }

            /** Draw a filled rectrangle. */
            static void fillrect(geometry extent, color col) {
                if (extent.w == geometry::all)
                    extent.w = layout.w - extent.x;
                if (extent.h == geometry::all)
                    extent.h = layout.w - extent.y;

                screen.fillRect(extent.x, extent.y, extent.w, extent.h, std::underlying_type_t<color>(col));
            }

            /** Draw text on the display. */
            static int vputtext(position pos, align align_, const char *fmt, va_list args);
            static int puttext(position pos, align align_, const char *fmt, ...) _ATTRIBUTE ((__format__ (__printf__, 3, 0))) {
                va_list args;
                va_start(args, fmt);
                return vputtext(pos, align_, fmt, args);
            }

            //=== PAGES ===========================================================================================================
        public:
            enum class context {
                MAIN
            };

        public:
            static context __context;

            //--- MAIN ------------------------------------------------------------------------------------------------------------
            struct main {
                enum class select : int {
                    speed = 0,
                    strength = 1,

                    __last
                };

                static select __selected;

                static void handle();
            };
        };

        MUD_ENUMCLASS_BINARY_OPS(ui::align);

        /// Helper for in- or decrementing selected item.
        inline ui::main::select operator+(ui::main::select selected, int delta) {
            int current = std::underlying_type_t<ui::main::select>(selected);

            current += delta;
            if (current < 0)
                current = 0;
            else if (current >= std::underlying_type_t<ui::main::select>(ui::main::select::__last))
                current = std::underlying_type_t<ui::main::select>(ui::main::select::__last) - 1;

            return static_cast<ui::main::select>(current);
        }
    }
}

#endif /* MUD_SYSTEM_UI_HH_ */
