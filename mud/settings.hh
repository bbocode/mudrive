#ifndef MUD_SETTINGS_HH_
#define MUD_SETTINGS_HH_

#include "mud/assert.hh"
#include "mud/string.hh"

#include <functional>

namespace mud {
    /** System settings.
     *
     * This class provides an interface for retrieving and storing settings
     * which are either hard-coded or stored on EEPROM.
     *
     * NOTE: At time of writing, EEPROM store is not supported.
     * */
    class settings {
    public:
        /** Value type (variant). */
        class value_type {
        public:
            enum class nature : int {
                none = 0, integer, floating_point, string,
            };

        public:
            constexpr value_type() = default;
            constexpr value_type(int value) :
                    __nature(nature::integer), __data { .i = value } {
            }
            constexpr value_type(float value) :
                    __nature(nature::floating_point), __data { .f = value } {
            }
            constexpr value_type(const char *value) :
                    __nature(nature::string), __data { .s = value } {
            }

        protected:
            nature __nature = nature::none;
            union {
                int i;
                float f;
                const char *s;
            } __data = { .s = nullptr };

        public:
            /** Check if valid. */
            constexpr bool isvalid() const {
                return __nature != nature::none;
            }

            constexpr operator nature() const {
                return __nature;
            }

            constexpr operator int() const {
                assert(__nature == nature::integer);
                return __data.i;
            }

            constexpr operator uint8_t() const {
                assert(__nature == nature::integer);
                return (uint8_t) __data.i;
            }

            constexpr operator float() const {
                assert(__nature == nature::floating_point);
                return __data.f;
            }

            constexpr operator const char *() const {
                assert(__nature == nature::string);
                return __data.s;
            }

            constexpr operator string::view() const {
                assert(__nature == nature::string);
                return __data.s;
            }
        };

        /** Default key type. */
        typedef string::view key_type;

        /** Key-value pair.
         * */
        struct pair {
            key_type key;
            value_type value;
        };

    public:
        settings() = default;

    protected:
        static pair data[];             ///< Hard-coded (or initial) settings data (see config.cpp).

    public:
        value_type get(string::view key) const {
            // lookup in EEPROM
            // FIXME

            // lookup hard-coded
            for(int i=0; data[i].value.isvalid(); ++i) {
                if (data[i].key == key)
                    return data[i].value;
            }

            return value_type();
        }

        void set(string::view key, value_type value);

        value_type operator[](string::view key) const {
            return get(key);
        }
    };
}

#endif /* MUD_HAPTIC_CONFIG_HH_ */
