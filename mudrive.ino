/****************************************************************************
 Z-Drive controller

 Copyright (C) 2020 Research center caesar
 All rights reserved.

 FIXME

 ****************************************************************************/
#include "config.hh"

#include "mud/version.hh"
#include "mud/sysbeat.hh"
#include "mud/settings.hh"

#include "mud/system/protocol.hh"
#include "mud/system/sense.hh"
#include "mud/system/ui.hh"
#include "mud/system/buttons.hh"
#include "mud/system/led.hh"
#include "mud/system/stepper.hh"

using namespace mud;
using LED = system::LED;

void setup() {
#if defined(__MUD_HAS_BOARD_LED)
    // see below
    pinMode(__MUD_HAS_BOARD_LED, OUTPUT);
#endif

    system::LED::setup();
    system::ui::setup();
    delay(2000);

    system::protocol::setup();
    system::sense::setup();
    system::buttons::setup();
    system::stepper::setup();

    LED::awake();
    LED::error(LED::pattern::off);
    LED::active(LED::pattern::off);
}

void loop() {
#if defined(__MUD_HAS_BOARD_LED)
    // blink continuously (stops, when an unresolvable error, such as an assert(), occurs)
    sysbeat::step<8>(
            []{ digitalWriteFast(__MUD_HAS_BOARD_LED, digitalReadFast(__MUD_HAS_BOARD_LED) == HIGH ? LOW : HIGH); }
    );
#endif

    // handle:
    // - serial communication
    // - voltage/current sensing
    // - leds and buttons
    // - display
    sysbeat::step<25, 0>(system::protocol::step);
    sysbeat::step<10, 1>(system::sense::step);
    sysbeat::step<system::buttons::rate, 2>(system::buttons::step);
    sysbeat::step<16 /* must be bit count of system::LED::pattern */>(system::LED::step);
    sysbeat::step<10, 3>(system::ui::step);
    sysbeat::step<system::stepper::rate, 4>(system::stepper::step);

    sysbeat::tick();
}
