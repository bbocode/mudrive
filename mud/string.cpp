#include "string.hh"

using namespace mud;

void string::__copy_heap(size_type sizehint) {
    // create copy
    size_type required = next_power_of_2(sizehint+__MUD_STRING_EXTRA);

    pointer buf = reinterpret_cast<pointer>(::malloc(required));
    assert(buf != nullptr);

    ::memcpy(buf, __data, __size);

    // assign
    __data = buf;
    __dynamic.capacity = required;
}

void string::__alloc_heap(size_type n) {
    assert(__data == nullptr);
    size_type required = next_power_of_2(n);
    assert(__data = reinterpret_cast<pointer>(::malloc(required)));
    __dynamic.capacity = required;
}

void string::__resize_heap(size_type n) {
    assert(!islocal() && !isstatic());
    size_type required = next_power_of_2(n);
    if (required > __dynamic.capacity) {
        assert(__data = reinterpret_cast<pointer>(::realloc(__data, required)));
        __dynamic.capacity = required;
    }
}

void string::resize(size_type sizehint) {
    assert(!isstatic());
    if (sizehint + __MUD_STRING_EXTRA < sso)
        return;
    __resize_heap(sizehint + __MUD_STRING_EXTRA);
}
