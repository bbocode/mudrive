#ifndef MUD_SYSTEM_PROTOCOL_HH_
#define MUD_SYSTEM_PROTOCOL_HH_

#include <Arduino.h>

#include "mud/string.hh"
#include "mud/system/led.hh"

#define __MUD_PROTOCOL_ERROR_BUFSIZE 1024

namespace mud {
    namespace system {
        /** Communication protocol handler. */
        class protocol {
        public:
            protocol() = default;

            /** Communication stream (Serial by default). */
            static Stream &io;

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

        public:
            static int write(const char *str) {
                return io.write(str);
            }

            static int vrespond(const char *fmt, va_list ap) {
            #ifdef __STRICT_ANSI__
                return 0;  // TODO: make this work with -std=c++0x
            #else
                return vdprintf((int)&io, fmt, ap) + dprintf((int)&io, "\n");
            #endif
            }

            static int respond(string::view v) {
                return io.write(v.data(), v.length());
            }

            static int respond(const char *fmt, ...) _ATTRIBUTE ((__format__ (__printf__, 1, 0))) {
                va_list ap;
                va_start(ap, fmt);
                return vrespond(fmt, ap);
            }

            static int respond(int value) {
                return respond("%d", value);
            }

            static int respond(long value) {
                return respond("%ld", value);
            }

            static int respond(unsigned int value) {
                return respond("%u", value);
            }

            static int respond(unsigned long value) {
                return respond("%lu", value);
            }

            static int respond(float value) {
                return respond("%f", value);
            }

            static int vlog(const char *fmt, va_list ap);
            static int log(const char *fmt, ...) _ATTRIBUTE ((__format__ (__printf__, 1, 0))) {
                va_list ap;
                va_start(ap, fmt);
                return vlog(fmt, ap);
            }

        protected:
            static char __errorbuf[__MUD_PROTOCOL_ERROR_BUFSIZE]; ///< Error buffer.
            static unsigned __errorlen; ///< Error buffer fill count.

        public:
            static int verror(const char *fmt, va_list ap);
            static int error(const char *fmt, ...) _ATTRIBUTE ((__format__ (__printf__, 1, 0))) {
                va_list ap;
                va_start(ap, fmt);
                return verror(fmt, ap);
            }

            static void clearerror() {
                __errorlen = 0;
                system::LED::error(system::LED::pattern::off);
            }

        protected:
            static bool __move_flag;            ///< Set to true in __move command (for reporting end position).

            static bool __test_flag;
            static float __test_target;

            static string::view token(string::view &what);
            static bool tofloat(float &result, string::view &what);

        public:
            /** Execute a command. */
            static void command(string::view what);

        protected:
            static void __help(string::view what);
            static void __list(string::view what);
            static void __set(string::view what);
            static void __get(string::view what);
            static void __move(string::view what);
            static void __zero(string::view what);
            static void __reboot(string::view what);
            static void __stop(string::view what);
            static void __test(string::view what);

        private:
            static void __move_to(float target);
        };
    }
}

#endif /* MUD_SYSTEM_PROTOCOL_HH_ */
