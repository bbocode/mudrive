#ifndef MUD_BUTTON_HH_
#define MUD_BUTTON_HH_

#include <Arduino.h>

#include "mud/utils.hh"
#include "mud/system/led.hh"

namespace mud {
    /** Button helpers. */
    namespace button {
        /** Button capabilities. */
        namespace capability {
            /** Button to ground with internal pullup. */
            struct pullup {
                static constexpr uint8_t mode = INPUT_PULLUP;
                static constexpr uint8_t active = LOW;
            };
        }

        /** Simple pressed button detection (without debouncing).
         */
        template<uint8_t Pin, typename Capability = capability::pullup>
        class press {
        public:
            press() = default;

        public:
            void setup() {
                pinMode(Pin, Capability::mode);
            }

            operator bool() {
                return digitalReadFast(Pin) == Capability::active;
            }
        };

        /** Encoder.
         * NOTE: this assumes a "dented" encoder (such as Alps Alpine EC12E). */
        template<uint8_t PinA, uint8_t PinB, typename Capability = capability::pullup>
        class encoder {
        public:
            encoder() = default;

        private:
            static volatile int __count;        ///< Dented encoder count.
            static volatile bool __paused;      ///< Encoder detection is paused.
            static volatile int __step;         ///< FIXME

            static int a() {
                return digitalReadFast(PinA) == Capability::active ? 1 : 0;
            }

            static int b() {
                return digitalReadFast(PinB) == Capability::active ? 1 : 0;
            }

            static inline void __changed() {
                int dir = sgn(__step);
                if (dir * __step == 3) // encoder is not reliable on dented positions (should be '> 3')
                    __count += dir;

                if ((a() == 0) && (b() == 0))
                    __step = 0;
            }

            static void __a_changed() {
                // count quarter steps
                if (a() ^ b()) ++__step;
                else --__step;

                // update dented counter;
                __changed();
            }

            static void __b_changed() {
                // count quarter steps
                if (a() ^ b()) --__step;
                else ++__step;

                // update dented counter
                __changed();
            }

        public:
            /** Setup function. */
            void setup() {
                pinMode(PinA, Capability::mode);
                pinMode(PinB, Capability::mode);
                wakeup();
            }

            /** Wake-up encoder. */
            void wakeup() {
                if (__paused) {
                    __paused = false;
                    attachInterrupt(digitalPinToInterrupt(PinA), __a_changed, CHANGE);
                    attachInterrupt(digitalPinToInterrupt(PinB), __b_changed, CHANGE);
                }
            }

            /** Pause encoder. */
            void pause() {
                if (!__paused) {
                    detachInterrupt(digitalPinToInterrupt(PinA));
                    detachInterrupt(digitalPinToInterrupt(PinB));
                    __paused = true;
                }
            }

            int peek() const {
                return __count;
            }

            operator int() {
                volatile int retval = __count;
                __count = 0;
                return retval;
            }

            int __getstep() {
                return __step;
            }
        };

#define MUD_BUTTON_ENCODER_IMPL(T) \
        template<> volatile int T::__count = 0; \
        template<> volatile bool T::__paused = true; \
        template<> volatile int T::__step = 0; \
        __MUD_SWALLOW_SEMICOLON

    }
}

#endif /* MUD_BUTTON_HH_ */
