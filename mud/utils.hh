#ifndef MUD_UTILS_HH_
#define MUD_UTILS_HH_

#include <type_traits>
#include <algorithm>

/** Expanded macro stringification.
 *
 * Example:
 * @code
 * __MUD_STRINGIFY_EXPAND(__LINE__)
 * @endcode
 * yields (for example)
 * @code
 * "43"
 * @endcode
 *
 * @see __MUD_STRINGIFY */
#define __MUD_STRINGIFY_EXPAND(expr) __MUD_STRINGIFY(expr)

/** Macro stringification.
 *
 * Example:
 * @code
 * __MUD_STRINGIFY(__LINE__)
 * @endcode
 * yields
 * @code
 * "__LINE__"
 * @endcode
 *
 * @see __MUD_STRINGIFY_EXPAND */
#define __MUD_STRINGIFY(expr) #expr

/** Helper for compound defines to avoid compiler confusion.
 * @see __MUD_ENUMCLASS_BINARY_OPERATOR */
#define __MUD_SWALLOW_SEMICOLON struct __swallow_semicolon__

/** Declare a binary operator (for an enum class).
 *
 * Example:
 * @code
 *     __MUD_ENUMCLASS_BINARY_OPERATOR(id, &)
 * @endcode
 *
 * will declare the binary '&' operator for an 'enum class id'.
 * @see MUD_ENUMCLASS_BINARY_OPS */
#define __MUD_ENUMCLASS_BINARY_OPERATOR(TYPE,OPERATION) \
        inline TYPE operator OPERATION(TYPE lhs, TYPE rhs) { \
            return static_cast<TYPE>( \
                std::underlying_type_t<TYPE>(lhs) \
                        OPERATION std::underlying_type_t<TYPE>(rhs) ); \
        } \
        inline TYPE &operator OPERATION##=(TYPE &lhs, TYPE rhs) { \
            lhs = lhs OPERATION rhs; \
            return lhs; \
        } \
        __MUD_SWALLOW_SEMICOLON

/** Declare a unary operator (for an enum class).
 *
 * Example:
 * @code
 *     __MUD_ENUMCLASS_UNARY_OPERATOR(id, &)
 * @endcode
 *
 * @see MUD_ENUMCLASS_BINARY_OPS */
#define __MUD_ENUMCLASS_UNARY_OPERATOR(T,OPERATION) \
    inline T operator OPERATION(T value) { \
        return static_cast<T>(OPERATION std::underlying_type_t<T>(value) ); \
    } \
    __MUD_SWALLOW_SEMICOLON

/** Declare binary operations for an 'enum class'.
 *
 * This sets up essential binary operators for an enum class:
 * @c &, @c |, @c ^, @c ~, and, the boolean not @c !.
 *
 * */
#define MUD_ENUMCLASS_BINARY_OPS(T) \
    __MUD_ENUMCLASS_BINARY_OPERATOR(T,&); \
    __MUD_ENUMCLASS_BINARY_OPERATOR(T,|); \
    __MUD_ENUMCLASS_BINARY_OPERATOR(T,^); \
    __MUD_ENUMCLASS_UNARY_OPERATOR(T,~); \
    inline bool operator!(T value) { \
        return 0 == std::underlying_type_t<T>(value); \
    } \
    __MUD_SWALLOW_SEMICOLON

namespace mud {
    /** Sign of an integral number.
     * @returns +1 if positive, 0 if zero, -1 if negative.
     * */
    template<typename T, std::enable_if_t<std::__and_<std::is_integral<T>, std::is_signed<T>>::value, int> = 0>
    inline int sgn(T val) {
        return (T(0) < val) - (val < T(0));
    }

    /** Meta pow().
     * @see https://stackoverflow.com/questions/1505675/power-of-an-integer-in-c */
    template<int X, int P>
    struct Pow {
        enum {
            result = X * Pow<X, P - 1>::result
        };
    };
    template<int X>
    struct Pow<X, 0> {
        enum {
            result = 1
        };
    };
    template<int X>
    struct Pow<X, 1> {
        enum {
            result = X
        };
    };

    /** Meta-template to round down value to 2^n-1.
     *
     * This is equivalent to writing
     * @code
     * x |= x >> 1;
     * x |= x >> 2;
     * x |= x >> 4;
     * ...
     * x |= x >> 16;
     * @endcode
     * for a 32-bit (unsigned) integer.
     * */
    template<typename T, size_t ShiftMax = sizeof(T), size_t Shift = 1>
    struct RoundDownPow2 {
        constexpr void operator()(T &x) {
            x |= x >> Shift;
            RoundDownPow2<T,ShiftMax,Shift*2>()(x);
        }
    };

    /** Stop condition. */
    template<typename T, size_t ShiftMax>
    struct RoundDownPow2<T, ShiftMax, ShiftMax> {
        constexpr void operator()(T &x) {
            x |= x >> ShiftMax;
        }
    };
    ///@}

    /** Find next highest power of 2.
     *
     * This function calculates the next highest power of 2 of a value.
     *
     * @note The result is undefined for ~0.
     *
     * @see https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
    ///@{
    template<typename T>
    constexpr inline T next_power_of_2(T x) {
        RoundDownPow2<T>()(--x);
        return ++x;
    }
    ///@}

    /** Clip to min-max. */
    template<typename T>
    constexpr inline int clip(T val, T lower, T upper) {
        return std::max(std::min(val, upper), lower);
    }
}

#endif /* MUD_UTILS_HH_ */
