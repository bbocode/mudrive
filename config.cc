#include "config.hh"
#include "mud/settings.hh"
#include "mud/version.hh"

mud::settings::pair mud::settings::data[] = {
        {"version",  MUD_VERSION_S },

        // motor characteristics (Faulhaber 0308B)
        {"motor.voltage.nominal", 3.0F /* [V] */ },
        {"motor.current.max", 0.100F /* [A] */}, // FIXME
        {"motor.phases", 1},
        {"motor.gear_ratio", 125},

        // drive characteristics (Servo Amplifier BLD 05002 S)
        {"drive.steps_per_pole", 48 },
        {"drive.frequency.max", 120000.0F /* [Hz] */},
        {"drive.frequency.min", 12.5F     /* [Hz] */},
        {"drive.voltage.factor", 5.0F * __MUD_ANALOG_F /* [V/1] */ },
        {"drive.voltage.offset", 0.0F /* [V] */},

        // spindle characteristics (Faulhaber FIXME)
        {"spindle.pitch", 200.0F /* [um] */},

        // voltage/current sense calibration
        {"sense.supply_voltage.factor", 5.0F * __MUD_ANALOG_F /* [V/1] */ },
        {"sense.supply_voltage.offset", 0.0F /* [V] */ },
        {"sense.drive_current.factor", 1.0F * __MUD_ANALOG_F /* [A/1] */ },
        {"sense.drive_current.offset", 0.0F /* [A] */ },

        // objective
        {"objective.lever_ratio", 0.5F },
        {"objective.speed",      10.0F   /* [um/s] */ },
        {"objective.speed.max", 100.0F   /* [um/s] */ },
        {"objective.speed.min",   0.010F /* [um/s] */ },
        {"objective.center", 800.0F /* [um] from top */},

        {}
};
