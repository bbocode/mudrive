#include "buttons.hh"
#include "mud/system/stepper.hh"
#include "mud/system/protocol.hh"

using namespace mud;

using stepper = mud::system::stepper;
using io = mud::system::protocol;

MUD_BUTTON_ENCODER_IMPL(system::buttons::encoder_type);

void system::buttons::setup() {
    reset.setup();
    select.setup();
    up.setup();
    down.setup();
    encoder.setup();
}

void system::buttons::step() {
#define RATE_MILLIS (1000/rate)
    static int elapsed = 0;
    static bool waitfor = false;
    static bool done = false;

    // increment time
    if (waitfor) {
        elapsed += RATE_MILLIS;

        if (select) {
            if (encoder.peek()) {
                done = true;
            }
            else if (!done && (elapsed >= 1000)) {
                stepper::set_zero();
                done = true;
            }
        }
        else if (up && down) {
            if (!done && (elapsed >= 1000)) {
                stepper::set_center();
                done = true;
            }
        }
        else {
            waitfor = false;
            elapsed = 0;
            done = false;
        }
    }
    else {
        if (select)
            waitfor = true;
        else if (up && down)
            waitfor = true;
    }
}


