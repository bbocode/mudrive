#ifndef MUD_SYSTEM_SENSE_HH_
#define MUD_SYSTEM_SENSE_HH_

#include <Arduino.h>

#include "mud/analog.hh"

#define __MUD_ANALOG_AVERAGE_N 16

namespace mud {
    namespace system {
        /** System unit for sensing (here, supply voltage and drive current).
         */
        class sense {
        public:
            /** Available sensors. */
            enum {
                SUPPLY_VOLTAGE = 0,
                DRIVE_CURRENT = 1,

                __SENSORS__ = 2,
            };

        public:
            sense() = default;

        public:
            static analog::calibration __supply_voltage_calib;
            static analog::calibration __drive_current_calib;

        protected:
            static int __supply_voltage;
            static int __drive_current;

            static int __index;

            static int __supply_voltage_buf[__MUD_ANALOG_AVERAGE_N];
            static int __drive_current_buf[__MUD_ANALOG_AVERAGE_N];

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

            /** Get supply voltage. */
            static float supply_voltage();

            /** Get drive current. */
            static float drive_current();
        };
    }
}

#endif /* MUD_SYSTEM_SENSE_HH_ */
