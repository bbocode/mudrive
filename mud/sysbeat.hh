#ifndef MUD_SYSBEAT_HH_
#define MUD_SYSBEAT_HH_

#include "Arduino.h"

#include <functional>

#define MUD_SYSBEAT_FREQUENCY 1000
namespace mud {
    namespace sysbeat {
        inline void tick() {
            static unsigned long __systick_millis_count = 0;

            // wait for millis() to increment
            // note: we ignore the fact, that step() below could take more than a millisecond!
            unsigned long current_systicks = millis();
            for(;__systick_millis_count == current_systicks; current_systicks = millis());
            __systick_millis_count = current_systicks;
        }

        /** Execute a command (with a given @a Rate). */
        template<unsigned int Rate, unsigned int Offset = 0>
        void step(std::function<void()> callback) {
            static unsigned int __count = Offset;

            if (++__count >= (MUD_SYSBEAT_FREQUENCY/Rate)) {
                __count = 0;
                callback();
            }
        }
    }
}

#endif /* MUD_SYSBEAT_HH_ */
