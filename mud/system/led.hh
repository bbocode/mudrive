/* 
 * Copyright (C) 2020 COPYRIGHT_HOLDER
 * Author(s):
 *     AUTHOR_NAME
 * License: LGPL v3 (see accompanying LICENSE file).
 *
 */
/** @file */
#ifndef MUD_SYSTEM_LED_HH_
#define MUD_SYSTEM_LED_HH_

#include <Arduino.h>

#include "config.hh"

namespace mud {
    namespace system {

        /** LEDs.
         */
        class LED {
        public:
            static constexpr int N = 3; ///< LED count.

            /** LED notifications.
             * @see pin */
            enum class notify {
                error = 0,              ///< Error LED index.
                active = 1,             ///< Activity LED index.
                awake = 2               ///< System awake (and good) LED index.
            };

            /** LED blink pattern. */
            enum class pattern : uint16_t {
                off             = 0,
                on              = 0b1111111111111111,
                blink           = 0b0000000011111111,
                fastblink       = 0b0101010101010101,
                flash           = 0b0000000000000011,
            };

            static constexpr const uint8_t pin[N] = { config::LED::pin::red, config::LED::pin::green, config::LED::pin::blue };

        protected:
            static uint16_t __shift;
            static volatile pattern __pattern[N];

        protected:
            static void __write(int led, bool on) {
                digitalWriteFast(pin[led], on ? HIGH : LOW);
            }

            static void __set(notify led, pattern value) {
                __pattern[std::underlying_type_t<notify>(led)] = value;
            }

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

        public:
            /** Set awake LED.
             * The "awake" LED is usually constantly on. */
            static void awake(pattern value = pattern::on) {
                __set(notify::awake, value);
            }
            /** Set activity LED.
             * The "activity" LED is on while the motor moves. */
            static void active(pattern value) {
                __set(notify::active, value);
            }
            /** Set error LED.
             * Signals that some error occurred (which must be cleared through
             * the serial interface, preferably after reading the error message). */
            static void error(pattern value) {
                __set(notify::error, value);
            }
        };

        MUD_ENUMCLASS_BINARY_OPS(LED::pattern);
    }
}

#endif /* MUD_SYSTEM_LED_HH_ */
