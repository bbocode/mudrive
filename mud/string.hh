#ifndef MUD_STRING_HH_
#define MUD_STRING_HH_

#include "config.hh"
#include "mud/assert.hh"

#include <cstring>

#define __MUD_STRING_EXTRA 1 /* space for NUL-termination */

namespace mud {
    /** Character buffer.
     * Simple and small std::string equivalent character buffer implementation.
     *  */
    class string {
    public:
        using size_type = unsigned int;
        using value_type = char;
        using pointer = char *;
        using const_pointer = const char *;
        using reference = char *;
        using const_reference = const char *;
        using iterator = char *;
        using const_iterator = const char *;

        constexpr static const size_type sso = MUD_STRING_SMALL_SIZE; ///< Small string optimization (SSO) size.

        constexpr static const size_type npos = size_type(-1);

    public:
        /** String view.
         * This is a simple (and mutilated) form of C++'s std::string::view.
         *  */
        class view {
        public:
            using size_type = unsigned int;
            using const_pointer = const char *;

            constexpr static const size_type npos = size_type(-1);

        public:
            constexpr view() = default;
            constexpr view(const char *str) : __data(str), __size(::strlen(str)) {}
            constexpr view(const_pointer data, size_type length) : __data(data), __size(length) {}

        protected:
            const_pointer __data = nullptr;
            size_type __size = 0;

        public:
            constexpr const_pointer data() const {
                return __data;
            }

            constexpr size_type length() const {
                return __size;
            }

            constexpr bool isempty() const {
                return __size == 0;
            }

            view substr(size_type pos, size_type len = npos) const {
                assert(pos <= __size);
                len = (len <= __size-pos) ? len : __size - pos;
                return { __data + pos, len };
            }

            constexpr bool operator==(view other) const {
                return (__size == other.__size) ? (0 == ::memcmp(__data, other.__data, __size)) : false;
            }

            constexpr bool operator!=(view other) const {
                return (__size == other.__size) ? (0 != ::memcmp(__data, other.__data, __size)) : true;
            }

            constexpr size_type find_first(char c) const {
                for(size_type i = 0; i < __size; ++i) {
                    if (__data[i] == c)
                        return i;
                }
                return npos;
            }
        };

    public:
        string() : __data(&__local[0]) {}
        string(size_type capacity) {
            __alloc_heap(capacity);
        }
        string(const char *cstr) : __data(const_cast<pointer>(cstr)), __size(::strlen(cstr)) {}
        string &operator=(const char *cstr) {
            __release();
            __size = ::strlen(cstr);
            __data = const_cast<pointer>(cstr);
            __dynamic.capacity = 0;
            return *this;
        }

        string(view v);
        string &operator=(view other);

        string(const string &other);
        string &operator=(const string &other);

        string(string &&other);
        string &operator=(string &&other);

        ~string() {
            __release();
        }

    protected:
        pointer __data = nullptr;
        size_type __size = 0;
        union {
            char __local[sso] = {};             ///< Local (small string) buffer.
            struct {
                size_type capacity = 0;         ///< Capacity of the buffer.
            } __dynamic;
        };

    protected:
        void __release() {
            if (!islocal() && !isstatic())
                ::free(__data);
        }

        void __copy_local() noexcept {
            ::memcpy(__data, &__local[0], __size);
            __data = &__local[0];
        }

        void __copy_heap(size_type sizehint);

        void __alloc_heap(size_type n);

        void __resize_heap(size_type n);

    public:
        /** Check if local (internal) buffer is used.
         * */
        bool islocal() const {
            return __data == &__local[0];
        }

        /** Check if buffer content is a static C-string.
         * */
        bool isstatic() const {
            return !islocal() && (__dynamic.capacity == 0);
        }

    public:
        /** Represent as string::view. */
        operator view() const {
            return {__data,__size};
        }

        view substr(size_type pos, size_type len = npos) const {
            return operator view().substr(pos, len);
        }

        /** Direct data access. */
        const_pointer data() const {
            return __data;
        }

        /** Get string length. */
        size_type length() const {
            return __size;
        }

        /** Get capacity. */
        size_type capacity() const {
            return (islocal() ? sso : __dynamic.capacity) - __MUD_STRING_EXTRA;
        }

        /** Clear buffer.
         * This only sets the length to 0.
         */
        void clear() {
            assert(!isstatic());
            __size = 0;
        }

        /** Get null-terminated "C-string". */
        const char *cstr() const {
            if (!isstatic())
                __data[__size] = 0;
            return __data;
        }

        /** Resize buffer. */
        void resize(size_type sizehint);

        //FIXME erase
        //FIXME insert
        //FIXME append

        string &operator+=(value_type ch) {
            if (__size >= capacity())
                resize(__size+1);

            __data[__size++] = ch;

            return *this;
        }

        value_type operator[](unsigned int i) const {
            assert(i < length());
            return __data[i];
        }

        bool operator==(view v) const {
            return operator view() == v;
        }
        bool operator!=(view v) const {
            return operator view() != v;
        }
    };
}


#endif /* MUD_STRING_HH_ */
