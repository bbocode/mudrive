#include "protocol.hh"
#include "mud/version.hh"
#include "mud/string.hh"

#include "mud/system/stepper.hh"

#define BEL 0x07 // ASCII BEL

using namespace mud;

using stepper = mud::system::stepper;

Stream &mud::system::protocol::io = Serial;

void system::protocol::setup() {
    Serial.begin(9600);

    // announce
    delay(100);
    io.printf("|muDrive " MUD_RELEASE_S "\n");
}

inline bool iseol(char ch) {
    return (ch == '\n') || (ch == '\r');
}

void system::protocol::step() {
    static string inbuf(64); // input buffer

    if (io.available()) {
        int ch = io.read();

        if (ch < 0) {
            // FIXME report error ?
        }
        else if (iseol(ch)) {
            if (inbuf.length()) {
                (void) inbuf.cstr(); // enforce NUL termination (required, for example, for strtof())
                command(inbuf);
            }
            inbuf.clear();
        }
        else if (!isprint(ch)) {
            // FIXME report error ?
        }
        else if (isspace(ch)) {
            // squeeze spaces
            if ((inbuf.length() == 0) || !isspace(inbuf[inbuf.length()-1]))
                inbuf += ' ';
        }
        else
            inbuf += char(ch);
    }
    else {
        switch (stepper::get_drive_state()) {
            case stepper::state::moving:
                // always report position when moving
                respond("@%f", stepper::get_position());
                break;

            case stepper::state::ready:
                // report when end position reached
                if (__move_flag) {
                    respond("@%f", stepper::get_position());
                    __move_flag = false;
                }

                // TEST (this goes up and down, forever)
                if (__test_flag) {
                    float curpos = stepper::get_position();

                    if (curpos == __test_target)
                        __move_to(0.0F);
                    else
                        __move_to(__test_target);
                }

                break;

            default:
                break;
        }
    }
}

int system::protocol::vlog(const char *fmt, va_list ap) {
#ifdef __STRICT_ANSI__
    return 0;  // TODO: make this work with -std=c++0x
#else
    return dprintf((int)&io, "|LOG ") + vdprintf((int)&io, fmt, ap);
#endif
}

char system::protocol::__errorbuf[__MUD_PROTOCOL_ERROR_BUFSIZE];
unsigned system::protocol::__errorlen;

int system::protocol::verror(const char *fmt, va_list ap) {
    // emit error signal
    system::LED::error(system::LED::pattern::on);
    io.write(BEL);
    io.flush();

    // store error message in buffer
    int retval = vsnprintf(__errorbuf, __MUD_PROTOCOL_ERROR_BUFSIZE-__errorlen, fmt, ap);
#if defined(DEBUG)
    write("!ERROR ");
    for(int i=0; i<retval; ++i) {
        char ch = __errorbuf[__errorlen+i];
        io.write(ch);

        if ((i < retval-1) && (ch == '\n'))
            write("!ERROR ");
    }
    if (__errorbuf[__errorlen+retval-1] != '\n')
        io.write('\n');
#endif
    __errorlen += retval;
    return retval;
}

string::view system::protocol::token(string::view &what) {
    string::view::size_type next = what.find_first(' ');

    string::view retval = what.substr(0, next);
    if (next != string::npos)
        what = what.substr(next+1);
    else
        what = string::view();

    return retval;
}

bool system::protocol::tofloat(float &result, string::view &what) {
    // convert float
    char *eptr;
    result = strtof(what.data(), &eptr);

    // result?
    if (eptr > what.data()) {
        what = what.substr(eptr-what.data());
        return true;
    }
    else
        return false;
}

void system::protocol::command(string::view what) {
    string::view tok = token(what);

    if (tok == "help") {
        __help(what);
    }
    else if (tok == "list") {
        __list(what);
    }
    else if (tok == "get") {
        __get(what);
    }
    else if (tok == "set") {
        __set(what);
    }
    else if (tok == "move") {
        __move(what);
    }
    else if (tok == "stop") {
        __stop(what);
    }
    else if (tok == "zero") {
        __zero(what);
    }
    else if (tok == "reboot") {
        __reboot(what);
    }
    else if (tok == "test") {
        __test(what);
    }
    else
        error("Invalid command:\n    %s\n", tok.data());
}

void system::protocol::__help(string::view what) {
    if (!what.isempty()) {
        error("help does not take a parameter.");
    }
    else
        io.printf(
                "help                   This help text.\n"
                "list\n                 List parameters.\n"
                "set \"param\" <val>    Set parameter value.\n"
                "get \"param\"          Get a parameter value.\n"
                "move <value>           Move to position (um).\n"
                "stop                   Interrupt a move.\n"
                "zero                   Zero axes.\n"
                "reboot                 Reboot system\n"
                );
}

void system::protocol::__list(string::view what) {
    if (!what.isempty()) {
        error("list does not take a parameter.");
    }
    else
        io.printf(
                "R      error           Get stored error message (and clear error status).\n"
                "RW     mode            Drive mode ('automatic', 'manual').\n"
                "RW     speed           Drive speed (um/s).\n"
                "R      state           Drive state.\n"
                "RW     strength        Drive strength (%%).\n"
                "R      version         Firmware version.\n"
                );
}

void system::protocol::__set(string::view what) {
    string::view tok = token(what);
    if (what.isempty()) {
        error("set %s <value>: expected a value", tok.data());
        return;
    }

    if (tok == "mode") {
        if (what == "automatic")
            stepper::set_drive_mode(stepper::mode::automatic);
        else if (what == "manual")
            stepper::set_drive_mode(stepper::mode::manual);
        else {
            error("set mode %s: invalid mode ('automatic' or 'mode')", what.data());
            return;
        }
    }
    else if (tok == "speed") {
        float value;
        if (tofloat(value, what)) {
            stepper::set_speed(value);
        }
        else {
            error("set speed <value>: parameter value missing or invalid type");
            return;
        }
    }
    else if (tok == "strength") {
        float value;
        if (tofloat(value, what)) {
            stepper::set_drive_strength(value * 0.01F);
        }
        else {
            error("set strength <value>: parameter value missing or invalid type");
            return;
        }
    }
    else {
        error("set %s: unknown or read-only parameter", tok.data());
        return;
    }

    __get(tok);
}

void system::protocol::__get(string::view what) {
    string::view tok = token(what);
    if (!what.isempty()) {
        error("get %s: invalid token after parameter", tok.data());
        return;
    }

    if (tok == "error") {
        if (__errorlen > 0) {
            io.write(__errorbuf, __errorlen);
            io.write('\n');
            io.flush();
        }
        clearerror();
    }
    else if (tok == "mode") {
        switch(stepper::get_drive_mode()) {
            case stepper::mode::automatic:
                respond("automatic");
                break;
            case stepper::mode::manual:
                respond("manual");
                break;
        }
    }
    else if (tok == "speed")  {
        respond(stepper::get_speed());
    }
    else if (tok == "state") {
        switch(stepper::get_drive_state()) {
            case stepper::state::boot:
                respond("boot");
                break;
            case stepper::state::sleep:
                respond("sleep");
                break;
            case stepper::state::wakeup:
                respond("wakeup");
                break;
            case stepper::state::ready:
                respond("ready");
                break;
            case stepper::state::moving:
                respond("moving");
                break;
            case stepper::state::stop:
                respond("stop");
                break;
            case stepper::state::wait:
                respond("wait");
                break;
        }
    }
    else if (tok == "strength") {
        respond(stepper::get_drive_strength() * 100.0F);
    }
    else if (tok == "version") {
        respond(MUD_VERSION_S);
    }
    else
        error("get %s: unknown parameter", tok.data());
}

bool system::protocol::__move_flag = false;

void system::protocol::__move_to(float target) {
    // send to stepper
    float expected_target = stepper::set_position(target);

    // report target position
    respond(">%f", expected_target);

    // report when end position reached
    __move_flag = true;
}

void system::protocol::__move(string::view what) {
    string::view tok = token(what);
    if (!what.isempty()) {
        error("move %s: invalid token after parameter", tok.data());
        return;
    }

    float pos;
    if (!tofloat(pos, tok)) {
        error("move %s: expected a floating point value", tok.data());
        return;
    }

    __move_to(pos);
}

void system::protocol::__zero(string::view what) {
    if (!what.isempty()) {
        error("zero: invalid token after command", what.data());
        return;
    }

    stepper::set_zero();
}

void system::protocol::__stop(string::view what) {
    stepper::stop();
    __test_flag = false;
}

void system::protocol::__reboot(string::view what) {
    _reboot_Teensyduino_();
}

bool system::protocol::__test_flag = false;
float system::protocol::__test_target = 0.0F;

void system::protocol::__test(string::view what) {
    string::view tok = token(what);
    if (!what.isempty()) {
        error("test %s: invalid token after parameter", tok.data());
        return;
    }

    float pos;
    if (!tofloat(pos, tok)) {
        error("test %s: expected a floating point value", tok.data());
        return;
    }

    // send to stepper
    __test_target = stepper::set_position(pos);

    // report target position
    respond(">%f", __test_target);

    // report when end position reached
    __test_flag = true;
    __move_flag = true;
}

