#ifndef MUD_SYSTEM_STEPPER_HH_
#define MUD_SYSTEM_STEPPER_HH_

#include <Arduino.h>
#include <IntervalTimer.h>

#include "config.hh"
#include "mud/utils.hh"
#include "mud/settings.hh"

#include "mud/system/protocol.hh"
using io = mud::system::protocol;

namespace mud {
    namespace system {

        /** Stepper motor control (BLD 05002 S).
         */
        class stepper {
        public:
            static constexpr const int rate = 50;

            static constexpr struct {
                uint8_t dir;
                uint8_t clk;
            } pin = { config::drive::pin::dir, config::drive::pin::clk };       ///< Drive pins.

            static constexpr uint8_t dir_forward = config::drive::dir::forward; ///< Signal level for "forward" direction.

            static constexpr uint8_t clk_edge = config::drive::clk::edge;       ///< Clock edge when stepper advances.

        public:
            stepper() = default;

        public:
            /** Setup function. */
            static void setup();

            /** Loop step function. */
            static void step();

            //=== timer interrupt
        protected:
            static IntervalTimer __timer;                           ///< IRQ timer interface.
            static unsigned int __period;                           ///< Timer period (µs).

            static unsigned int __getperiod(float f) {              ///< Calculate timer period (twice for high/low transitions)
                return (unsigned int) (0.5F + 500000.0F / clip(f, __drive_frequency_min, __drive_frequency_max));
            }

            static void __setup() {                                 ///< Setup timer.
                __start();
            }

            static void __start() {                                 ///< Start (or wake-up) timer.
                // note: we flip HIGH/LOW outputs every __step (therefore 2*frequency).
                __timer.begin(isr, __period);
            }

            static void __stop() {                                  ///< Stop (or pause) timer.
                __timer.end();
            }

            static float __getfrequency() {
                return 500000.0F / float(__period);
            }

            static void __setfrequency(float rate) {                ///< Set timer frequency.
                __timer.update(__period = __getperiod(rate));
            }

            static void isr();                                      ///< Timer step function (ISR).

            //=== drive control logic
        protected:
            static volatile float __drive_frequency_max;            ///< Maximum stepper driver clock frequency.
            static volatile float __drive_frequency_min;            ///< Minimum stepper driver clock frequency.

            static volatile int __drive_dir;
            static volatile int __drive_count;
            static volatile int __drive_target;

            /**@internal Set drive direction. */
            static void __setdirection(int dir) __attribute__((always_inline, unused)) {
                __drive_dir = sgn(dir);
                if (dir_forward == HIGH)
                    digitalWriteFast(pin.dir, (dir > 0) ? HIGH : LOW);
                else
                    digitalWriteFast(pin.dir, (dir > 0) ? LOW : HIGH);
            }

            /**@internal Get current clock signal level. */
            static uint8_t __clocklevel() __attribute__((always_inline, unused)) {
                return digitalReadFast(pin.clk);
            }

            /**@internal Clock signal default state. */
            static uint8_t __clockdefault() __attribute__((always_inline, unused)) {
                switch (clk_edge) {
                    case RISING:
                        return LOW;
                    case FALLING:
                        return HIGH;
                }
                // never reached - make compiler happy
                return uint8_t(-1);
            }

            /**@internal Toggle clock level (make a transition from HIGH to LOW or vice versa).
             * @return @c true if counting edge is triggered, @c false otherwise */
            static bool __clocktoggle() __attribute__((always_inline, unused)) {
                uint8_t lvl = digitalReadFast(pin.clk);
                digitalWriteFast(pin.clk, (lvl == HIGH) ? LOW : HIGH);
                return lvl == __clockdefault();
            }

            /**@internal Bring clock signal level back to default. */
            static void __clockreturn() __attribute__((always_inline, unused)) {
                if (__clocklevel() != __clockdefault())
                    (void) __clocktoggle();
            }

            /**@internal Make a clock signal transition (from LOW to HIGH and vice versa).
             * @return @c true if edge increments */
            static void __clocktick() __attribute__((always_inline, unused)) {
                if (__clocktoggle()) {
                    __drive_count += __drive_dir;
                }
            }

            //=== motor control logic
        public:
            /** Drive strength mode. */
            enum class mode {
                automatic, manual,
            };

            /** Drive state. */
            enum class state {
                boot,           ///< First state on boot (only executed once).
                sleep,          ///< In sleep mode (reduced output voltage).
                wakeup,         ///< Wakeup from sleep.
                ready,          ///< System ready.
                moving,         ///< Motors moving.
                stop,           ///< Stop moving.
                wait            ///< Wait for parameter updates.
            };

            /** Scope wait (see state::wait). */
            class wait {
            public:
                wait() : __state(__drive_state) {
                    __drive_state = state::wait;
                }
                ~wait() {
                    __drive_state = __state;
                }

            protected:
                state __state;
            };

        protected:
            static volatile mode __drive_mode;                      ///< Drive mode.
            static volatile state __drive_state;                    ///< Drive state.

            static volatile uint16_t __drive_strength;              ///< DAC value for current drive strength;

            static volatile float __drive_voltage_factor;           ///< Voltage set point conversion factor.
            static volatile float __drive_voltage_offset;           ///< Voltage set point conversion offset.

            static float __step_to_position;                        ///< Conversion factor from single step to position (um).
            static float __position_to_step;                        ///< Conversion factor from position (um) to single step.

        protected:
            static void __set_drive_strength(float value);

        public:
            /** Get drive state. */
            static state get_drive_state() {
                return __drive_state;
            }

            /** Get drive mode.
             * FIXME
             */
            static mode get_drive_mode() {
                return __drive_mode;
            }

            /** Set drive mode. */
            static void set_drive_mode(mode mode_) {
                __drive_mode = mode_;
                // FIXME
            }

            /** Get drive strength (1). */
            static float get_drive_strength();

            /** Set drive strength (1).
             * The @a value is ignored if in automatic mode. */
            static void set_drive_strength(float value) {
                if (__drive_mode == mode::manual) {
                    __set_drive_strength(value);
                }
            }

            /** Get objective speed (um/s). */
            static float get_speed() {
                return __getfrequency() * __step_to_position;
            }

            /** Set objective speed (um/s). */
            static void set_speed(float v);

        public:
            /** Interrupt movement. */
            static void stop() {
                __drive_state = state::stop;
            }

            /** Get current position of the objective (um). */
            static float get_position() {
                return float(__drive_count) * __step_to_position;
            }

            /** Set (or move to) a position (um).
             * @return Target position. */
            static float set_position(float z) {
                wait scope;

                __drive_target = z * __position_to_step;
                __setdirection(sgn(__drive_target-__drive_count));

                return float(__drive_target) * __step_to_position;
            }

            /** Set absolute zero. */
            static void set_zero() {
                wait scope;

                __drive_target = __drive_count = 0;
            }

            /** Got to center position (and zero). */
            static void set_center();
        };
    }
}

#endif /* MUD_SYSTEM_CONTROL_HH_ */
